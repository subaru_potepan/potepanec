require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  let!(:product) do
    create(:product,
           name: "RUBY ON RAILS SHIRT",
           description: "simple shirt",
           price: "20.00")
  end

  scenario "visiting product page" do
    visit potepan_product_path(product.id)

    expect(page).to have_content "RUBY ON RAILS SHIRT"
    expect(page).to have_content "simple shirt"
    expect(page).to have_content "$20.00"
  end
end
