require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let!(:product) { create(:product) }

    before { get :show, params: { id: product.id } }

    context "the product information is accurately displayed" do
      it "responds successfully" do
        expect(response.status).to eq(200)
      end

      it "@product has the expected value" do
        expect(assigns(:product)).to eq product
      end

      it "render the show template" do
        expect(response).to render_template :show
      end
    end
  end
end
